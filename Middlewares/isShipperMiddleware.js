const {Profile} = require('../models/profileModel');

module.exports.isShipper = async (req, res, next) => {
  const shipper = await Profile.findOne({_id: req.userId});
  if (shipper.role !== 'SHIPPER') {
    return res.status(400)
        .json({message: `You are not a shipper!`});
  }
  next();
};

