const joi = require('joi');
const {BadRequestError} = require('../errorClass');

const validatePassword = async (req, res, next) => {
  const schema = joi.object({
    oldPassword: joi.string().required(),
    newPassword: joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    // eslint-disable-next-line max-len
    throw new BadRequestError( 'Password must be specified by 6-30 characters and only letters and/or digits!');
  }
  next();
};

module.exports = {validatePassword};
