const joi = require('joi');
const {BadRequestError} = require('../errorClass');

const validateTruck = async (req, res, next) => {
  const schema = joi.object({
    type: joi.string()
        .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT').required(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }
  next();
};

module.exports = {validateTruck};
