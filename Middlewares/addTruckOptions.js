const {Profile} = require('../models/profileModel');

module.exports.isDriver = async (req, res, next) => {
  const driver = await Profile.findOne({_id: req.userId});
  if (driver.role !== 'DRIVER') {
    return res.status(400)
        .json({message: `You are not a driver!`});
  }
  next();
};

