# NODEJS_HW3

Description

In this project is implemented service (UBER like) for freight trucks, in REST style, using MongoDB as database. 
This service should help regular people to deliver their stuff and help drivers to find loads and earn some money. 
Application contains 2 roles, driver and shipper.

For more detailed information, see 'swagger.yaml'.