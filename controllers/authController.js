const joi = require('joi');
const {JWT_SECRET} = require('../configJwtSecret');
const {Profile} = require('../models/profileModel');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {BadRequestError} = require('../errorClass');
const generator = require('generate-password');
const nodemailer = require('nodemailer');

module.exports.register = async (req, res) => {
  const {email, password, role} = req.body;
  const profile = new Profile({
    email,
    password: await bcrypt.hash(password, 10),
    role,
  });

  try {
    await profile.save();
    res.json({message: 'Profile created successfully'});
  } catch {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `Such profile has already exists or your role is incorrect.`});
  }
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;

  if (!email) {
    return res.status(400).json({message: `Please, specify email`});
  }
  if (!password) {
    return res.status(400).json({message: `Please, specify password`});
  }

  const userFind = await Profile.find({email: email});
  const user = userFind[0];

  if (!user) {
    return res.status(400)
        .json({message: `User with email '${email}' wasn't found`});
  }

  if (!(await bcrypt.compare(password, user.password))) {
    return res.status(400).json({message: `Wrong password`});
  }

  const token = jwt.sign({email: user.email, _id: user._id}, JWT_SECRET);
  await Profile.updateOne({_id: user._id}, {$set: {token: token}});

  res.status(200).json({jwt_token: token});
};

module.exports.forgot_password = async (req, res) => {
  const {email} = req.body;

  if (!email) {
    return res.status(400).json({message: `Please, specify email`});
  }

  const user = await Profile.findOne({email: email});

  if (!user) {
    return res.status(400)
        .json({message: `User with email '${email}' wasn't found`});
  }

  const newPassword = generator.generate({
    length: 10,
    numbers: true,
  });
  const hashedPW = await bcrypt.hash(newPassword, 10);
  await sendMail(user.email, newPassword);
  await Profile.updateOne({_id: user._id}, {password: hashedPW});

  res.status(200).json({message: `New password sent to your email address.`});
};

module.exports.validateReg = async (req, res, next) => {
  const schema = joi.object({
    email: joi.string()
        .email({minDomainSegments: 2, tlds: {allow: ['com', 'net']}})
        .required(),
    password: joi.string().required(),
    role: joi.string().required(),
  });

  try {
    await schema.validateAsync(req.body);
  } catch (err) {
    throw new BadRequestError(err.message);
  }

  next();
};
// eslint-disable-next-line require-jsdoc
async function sendMail(email, newPW) {
  const testEmailAccount = await nodemailer.createTestAccount();
  const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    secure: false,
    auth: {
      user: testEmailAccount.user,
      pass: testEmailAccount.pass,
    },
  });
  await transporter.sendMail({
    from: '"HW3" <nodejs_hw3@example.com>',
    to: email,
    subject: 'New Password',
    text: `Your password was changed. New Password: ${newPW}.`,
    html: `<h1>Your password was changed. New Password: ${newPW}.</h1>`,
  });
}
