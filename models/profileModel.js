const mongoose = require('mongoose');
// eslint-disable-next-line new-cap
const profileSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ['SHIPPER', 'DRIVER'],
    required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  token: {
    type: String,
  },

}).index({email: 1}, {unique: true});


module.exports.Profile = mongoose.model('profile', profileSchema);
