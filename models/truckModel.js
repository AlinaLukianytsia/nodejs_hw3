const mongoose = require('mongoose');
// eslint-disable-next-line new-cap
const truckSchema = mongoose.Schema({
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: '',
  },
  type: {
    type: String,
    enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
    required: true,
  },
  status: {
    type: String,
    enum: ['OL', 'IS'],
    default: 'IS',
    // required: true,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
  specification: {
    type: Object,
    default: {},
  },
});


module.exports.Truck = mongoose.model('truck', truckSchema);
