const {isDriver} = require('../Middlewares/isDriverMiddleware');
const {Truck} = require('../models/truckModel');
const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {Load} = require('../models/loadModel');
const {Profile} = require('../models/profileModel');
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {isShipper} = require('../Middlewares/isShipperMiddleware');
// const {Truck} = require('../models/truckModel');

router.get('', asyncWrapper(async (req, res) => {
  const {status, skip, limit} = req.query;
  const requestOptions = {
    status: status,
    skip: parseInt(skip),
    limit: parseInt(limit)};

  const user = await Profile.findOne({_id: req.userId});
  if (user.role === 'SHIPPER') {
    const loads = await Load.find({created_by: req.userId},
        {__v: 0}, requestOptions);

    if (loads.length === 0) {
      // eslint-disable-next-line max-len
      return res.status(400).json({message: `You have no available loads now.`});
    }
    res.status(200).json({loads: loads});
  }
  if (user.role === 'DRIVER') {
    const loads = await Load.find({assigned_to: req.userId,
      $or: [{status: 'ASSIGNED'}, {status: 'SHIPPED'}]},
    {__v: 0}, requestOptions);

    if (loads.length === 0) {
      // eslint-disable-next-line max-len
      return res.status(400).json({message: `You have no assigned or shipped loads now.`});
    }
    res.status(200).json({loads: loads});
  }
},
));

router.post('/', asyncWrapper(isShipper), asyncWrapper(async (req, res) => {
  // eslint-disable-next-line camelcase,max-len
  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;
  const shipper = await Profile.findOne({_id: req.userId});
  const load = new Load({
    'created_by': shipper._id, name, payload,
    pickup_address, delivery_address, dimensions,
  });

  await load.save();
  res.status(200).json({message: 'Load created successfully'});
}));

router.put('/:id', asyncWrapper(isShipper), asyncWrapper(async (req, res) =>{
  // eslint-disable-next-line max-len,camelcase
  const {name, payload, pickup_address, delivery_address, dimensions} = req.body;

  // eslint-disable-next-line max-len
  const load = await Load.findOne({created_by: req.userId, _id: req.params.id},
      {__v: 0});

  if (!load) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have load with such id'});
  }
  if (load.status !== 'NEW') {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You can't change the posted load`});
  }
  await Load.updateOne({created_by: req.userId, _id: req.params.id},
      {name, payload, pickup_address, delivery_address, dimensions});
  res.status(200).json({message: 'Load details changed successfully'});
}));
// eslint-disable-next-line max-len
router.delete('/:id', asyncWrapper(isShipper), asyncWrapper(async (req, res) => {
  const load = await Load.findOne({created_by: req.userId, _id: req.params.id},
      {__v: 0});

  if (!load) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have load with such id'});
  }
  if (load.status !== 'NEW') {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You can't delete the posted load`});
  }
  await Load.deleteOne({created_by: req.userId, _id: req.params.id});

  res.status(200).json({message: 'Load deleted successfully'});
}));

router.post('/:id/post', asyncWrapper(isShipper),
    asyncWrapper(async (req, res) => {
      // eslint-disable-next-line max-len
      const load = await Load.findOne({created_by: req.userId, _id: req.params.id},
          {__v: 0});
      if (load.status !== 'NEW') {
        // eslint-disable-next-line max-len
        return res.status(400).json({message: `You can't post not new load`});
      }
      await Load.updateOne({_id: load._id}, {status: 'POSTED'});

      const loadW = load.dimensions.width;
      const loadL = load.dimensions.length;
      const loadH = load.dimensions.height;
      const loadP = load.payload;
      const truck = await Truck.findOne({
        'status': 'IS',
        'assigned_to': {$ne: ''},
        'specification.width': {$gte: loadW},
        'specification.length': {$gte: loadL},
        'specification.height': {$gte: loadH},
        'specification.payloadCapacity': {$gte: loadP},
      }, {__v: 0});

      if (!truck) {
        await Load.updateOne({_id: load._id}, {status: 'NEW'});
        return res.status(400)
            // eslint-disable-next-line max-len
            .json({message: `Sorry, there are no free valid trucks now. ${loadW}, ${truck}`});
      }
      const logObj = {
        message: `Load assigned to driver with id ${truck.assigned_to}`,
        time: new Date(),
      };
      await Truck.updateOne({_id: truck._id}, {status: 'OL'});
      await Load.updateOne({_id: load._id},
          {status: 'ASSIGNED', state: 'En route to Pick Up',
            assigned_to: `${truck.assigned_to}`, $push: {logs: logObj}});
      res.status(200)
          .json({message: 'Load posted successfully', driver_found: true});
    }));

router.get('/:id/shipping_info', asyncWrapper(isShipper),
    asyncWrapper(async (req, res) => {
      // eslint-disable-next-line max-len
      const load = await Load.findOne({created_by: req.userId, _id: req.params.id},
          {__v: 0});
      if (load.status !== 'ASSIGNED') {
        // eslint-disable-next-line max-len
        return res.status(400).json({message: `This load isn't active.`});
      }

      const truck = await Truck.findOne({assigned_to: load.assigned_to},
          {__v: 0});
      if (!truck) {
        return res.status(400)
            .json({message: `Sorry, the truck wasn't found.`});
      }

      res.status(200).json({load: load, truck: truck});
    }));

router.get('/active', asyncWrapper(isDriver),
    asyncWrapper(async (req, res) => {
      // eslint-disable-next-line max-len
      const load = await Load.findOne({assigned_to: req.userId, status: 'ASSIGNED'}, {__v: 0});
      if (!load) {
        return res.status(400)
            .json({message: `You have no active loads.`});
      }
      res.status(200).json({load: load});
    }));

router.patch('/active/state', asyncWrapper(isDriver),
    asyncWrapper(async (req, res) => {
      // eslint-disable-next-line max-len
      const load = await Load.findOne({assigned_to: req.userId, status: 'ASSIGNED'}, {__v: 0});
      switch (load.state) {
        case 'En route to Pick Up':
          const logObj = {
            message: `Load state changed to 'Arrived to Pick Up'`,
            time: new Date(),
          };
          await Load.updateOne({_id: load._id},
              {state: 'Arrived to Pick Up', $push: {logs: logObj}});
          res.status(200)
              .json({message: `Load state changed to 'Arrived to Pick Up'`});
          break;
        case 'Arrived to Pick Up':
          const logObj2 = {
            message: `Load state changed to 'En route to delivery'`,
            time: new Date(),
          };
          await Load.updateOne({_id: load._id},
              {state: 'En route to delivery', $push: {logs: logObj2}});
          res.status(200)
              .json({message: `Load state changed to 'En route to delivery'`});
          break;
        case 'En route to delivery':
          const logObj3 = {
            message: `Load state changed to 'Arrived to delivery'`,
            time: new Date(),
          };
          await Load.updateOne({_id: load._id},
              {state: 'Arrived to delivery', $push: {logs: logObj3}});
          const logObj4 = {
            message: `Load status changed to 'SHIPPED'`,
            time: new Date(),
          };
          await Load.updateOne({_id: load._id},
              {status: 'SHIPPED', $push: {logs: logObj4}});
          await Truck.updateOne({assigned_to: load.assigned_to},
              {status: 'IS'});
          res.status(200)
              .json({message: `Load state changed to 'Arrived to delivery'`});
          break;
      }
    }));


module.exports = router;
