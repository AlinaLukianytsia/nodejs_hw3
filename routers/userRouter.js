const bcrypt = require('bcrypt');
const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {Profile} = require('../models/profileModel');
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {validatePassword} = require('../Middlewares/validateNewPasword');

router.get('/', asyncWrapper(async (req, res) => {
  const currentUser = await Profile.findOne({_id: req.userId});
  if (!currentUser) {
    return res.status(400)
        .json({message: `Username '${currentUser}' wasn't found`});
  }

  res.status(200).json({
    user: {
      '_id': currentUser._id,
      'email': currentUser.email,
      'created_date': currentUser.createdDate,
    },
  });
}));

router.patch('/password', asyncWrapper(validatePassword),
    asyncWrapper(async (req, res) => {
      const {oldPassword, newPassword} = req.body;
      const currUser = await Profile.findOne({_id: req.userId});
      if (!currUser) {
        return res.status(400)
            .json({message: `User with id '${req.userId}' wasn't found`});
      }
      if (!(await bcrypt.compare(oldPassword, currUser.password))) {
      // eslint-disable-next-line max-len
        return res.status(400).json({message: 'Old password is wrong'});
      }
      if (newPassword === oldPassword) {
      // eslint-disable-next-line max-len
        return res.status(400).json({message: 'The new password is the same as the old one.'});
      }
      const newHashPassword = await bcrypt.hash(newPassword, 10);
      await Profile.updateOne({_id: currUser._id}, {password: newHashPassword});
      res.status(200).json({message: 'Password changed successfully'});
    }),
);

router.delete('/', asyncWrapper(async (req, res) => {
  const currentUser = await Profile.findOne({_id: req.userId});
  if (!currentUser) {
    return res.status(400)
        .json({message: `User with id '${req.userId}' wasn't found`});
  }
  if (currentUser.role==='DRIVER') {
    return res.status(400)
        .json({message: `Sorry, you can't delete your profile.`});
  }
  await Profile.deleteOne({_id: currentUser._id});
  res.status(200).json({message: 'Profile deleted successfully'});
}),
);

module.exports = router;
