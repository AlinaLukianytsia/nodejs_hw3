const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {Profile} = require('../models/profileModel');
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
const {validateTruck} = require('../Middlewares/validateTruck');
const {Truck} = require('../models/truckModel');
const sprinterInfo = {'width': 250, 'length': 300, 'height': 170,
  'payloadCapacity': 1700};
const smallStrInfo = {'width': 250, 'length': 500, 'height': 170,
  'payloadCapacity': 2500};
const largeStrInfo = {'width': 350, 'length': 700, 'height': 200,
  'payloadCapacity': 4000};

router.get('/', asyncWrapper(async (req, res) => {
  const trucks = await Truck.find({created_by: req.userId}, {__v: 0});

  res.status(200).json({trucks: trucks});
}));

router.post('/', asyncWrapper(validateTruck), asyncWrapper(async (req, res) => {
  const {type} = req.body;
  const driver = await Profile.findOne({_id: req.userId});
  // eslint-disable-next-line max-len
  const hasSuchTruck = await Truck.findOne({'created_by': driver._id, 'type': type});
  if (hasSuchTruck) {
    return res.status(400)
        .json({message: `You have registered already '${type}' truck`});
  }
  let truckSpecifications = {};
  switch (type) {
    // eslint-disable-next-line max-len
    case 'SPRINTER': truckSpecifications = Object.assign({}, sprinterInfo); break;
      // eslint-disable-next-line max-len
    case 'SMALL STRAIGHT': truckSpecifications = Object.assign({}, smallStrInfo); break;
      // eslint-disable-next-line max-len
    case 'LARGE STRAIGHT': truckSpecifications = Object.assign({}, largeStrInfo); break;
      // eslint-disable-next-line max-len
    default: return res.status(400).json({message: `Sorry, there are no truck '${truck.type}' info.`});
  }
  const truck = new Truck({'created_by': driver._id, type,
    'specification': truckSpecifications});
  await truck.save();
  res.status(200).json({message: 'Truck created successfully'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const truck = await Truck.findOne({created_by: req.userId,
    _id: req.params.id}, {__v: 0});

  if (!truck) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You don't have truck with such id`});
  }

  res.status(200).json({truck: truck});
}));

// eslint-disable-next-line max-len
router.put('/:id', asyncWrapper(validateTruck), asyncWrapper(async (req, res) => {
  const {type} = req.body;

  // eslint-disable-next-line max-len
  const truck = await Truck.findOne({created_by: req.userId, _id: req.params.id},
      {__v: 0});

  if (!truck) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have truck with such id'});
  }
  if (truck.status === 'OL') {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You can't change the loaded truck`});
  }
  let truckSpecifications = {};
  switch (type) {
    // eslint-disable-next-line max-len
    case 'SPRINTER': truckSpecifications = Object.assign({}, sprinterInfo); break;
      // eslint-disable-next-line max-len
    case 'SMALL STRAIGHT': truckSpecifications = Object.assign({}, smallStrInfo); break;
      // eslint-disable-next-line max-len
    case 'LARGE STRAIGHT': truckSpecifications = Object.assign({}, largeStrInfo); break;
      // eslint-disable-next-line max-len
    default: return res.status(400).json({message: `Sorry, there are no truck '${truck.type}' info.`});
  }
  await Truck.updateOne({created_by: req.userId, _id: req.params.id},
      {'type': type, 'specification': truckSpecifications});
  res.status(200).json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  // eslint-disable-next-line max-len
  const truck = await Truck.findOne({created_by: req.userId, _id: req.params.id},
      {__v: 0});

  if (!truck) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have truck with such id'});
  }
  if (truck.status === 'OL') {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You can't delete the loaded truck`});
  }
  await Truck.deleteOne({created_by: req.userId, _id: req.params.id});

  res.status(200).json({message: 'Truck deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
  const checkDriverTrucks = await Truck.find({created_by: req.userId,
    assigned_to: req.userId, status: 'OL'}, {__v: 0});

  if (checkDriverTrucks.length !== 0) {
    return res.status(400)
        // eslint-disable-next-line max-len
        .json({message: `You have already assigned other truck and it's loaded now.`});
  }

  const truck = await Truck.findOne({created_by: req.userId,
    _id: req.params.id}, {__v: 0});
  if (!truck) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: 'You don\'t have truck with such id'});
  }
  if (truck.assigned_to) {
    // eslint-disable-next-line max-len
    return res.status(400).json({message: `You have already assigned this truck`});
  }
  await Truck.updateOne({created_by: req.userId,
    assigned_to: req.userId}, {assigned_to: ''});
  await Truck.updateOne({created_by: req.userId, _id: req.params.id},
      {assigned_to: req.userId});
  res.status(200).json({message: 'Truck assigned successfully'});
}));

module.exports = router;
