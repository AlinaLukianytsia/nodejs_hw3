const {Router} = require('express');
// eslint-disable-next-line new-cap
const router = Router();
const {asyncWrapper} = require('../Middlewares/wrapperAsync');
// eslint-disable-next-line max-len,camelcase
const {login, register, forgot_password, validateReg} = require('../controllers/authController');

router.post('/register', asyncWrapper(validateReg), asyncWrapper(register));
router.post('/login', asyncWrapper(login));
router.post('/forgot_password', asyncWrapper(forgot_password));

module.exports = router;
